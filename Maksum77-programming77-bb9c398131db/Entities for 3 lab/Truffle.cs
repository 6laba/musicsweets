﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Truffle : AbstractSweet
    {
        private string _diameter;

        public string diameter
        {
            get { return _diameter; }
            set { _diameter = value; }
        }
    }
}
