﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class CaramelSweet : AbstractSweet
    {
        private double _hardness;

        public double hardness
        {
            get { return _hardness; }
            set { _hardness = value; }
        }
    }
}
