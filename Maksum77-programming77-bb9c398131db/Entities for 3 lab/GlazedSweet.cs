﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class GlazedSweet : CaramelSweet
    {
        private string _ThiknessOfGlaze;

        public string ThiknessOfGlaze
        {
            get { return _ThiknessOfGlaze; }
            set { _ThiknessOfGlaze = value; }
        }
    }
}
