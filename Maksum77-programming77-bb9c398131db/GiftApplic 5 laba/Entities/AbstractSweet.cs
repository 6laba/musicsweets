﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public abstract class AbstractSweet
    {
        private string _name;
        private int _weight;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public int Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }
        public AbstractSweet(string namefromabstr, int weightfromabstr)
        {
            Name = namefromabstr;
            Weight = weightfromabstr;
        }
    }
}
