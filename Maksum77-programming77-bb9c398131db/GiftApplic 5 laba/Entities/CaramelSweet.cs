﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class CaramelSweet : AbstractSweet
    {
        private int _hardness;

        public int hardness
        {
            get { return _hardness; }
            set { _hardness = value; }
        }
        public CaramelSweet(string namefromabstr, int weightfromabstr, int hardness1) : base(namefromabstr, weightfromabstr)
        {
            hardness = hardness1;
        }
    }
}
