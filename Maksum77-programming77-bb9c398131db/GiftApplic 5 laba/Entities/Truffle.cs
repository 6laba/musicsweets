﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Truffle : AbstractSweet
    {
        private int _diameter;

        public int diameter
        {
            get { return _diameter; }
            set { _diameter = value; }
        }
        public Truffle(string namefromabstr, int weightfromabstr, int diametr) : base(namefromabstr, weightfromabstr)
        {
            diameter = diametr;
        }
    }
}
