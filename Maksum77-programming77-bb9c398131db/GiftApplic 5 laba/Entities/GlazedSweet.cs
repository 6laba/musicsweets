﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class GlazedSweet : CaramelSweet
    {
        private int _ThiknessOfGlaze;

        public int ThiknessOfGlaze
        {
            get { return _ThiknessOfGlaze; }
            set { _ThiknessOfGlaze = value; }
        }
        public GlazedSweet (string namefromabstr, int weightfromabstr,int hardness1, int thikGlaze1) : base(namefromabstr, weightfromabstr,hardness1)
        {
            ThiknessOfGlaze = thikGlaze1;
        }
    }
}
