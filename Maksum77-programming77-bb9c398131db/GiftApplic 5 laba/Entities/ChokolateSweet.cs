﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class ChokolateSweet : AbstractSweet
    {
        private string _strew;

        public string strew
        {
            get { return _strew; }
            set { _strew = value; }
        }
        public ChokolateSweet(string namefromabstr, int weightfromabstr, string strew1) : base(namefromabstr, weightfromabstr)
        {
            strew = strew1;
        }
    }
}
