﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 

namespace Entities
{
    public class Present
    {
        private List<AbstractSweet> _gift = new List<AbstractSweet>();// ПОЛЕ!!!

        public void AddSweet(AbstractSweet sweet)
        {
            _gift.Add(sweet);
        }

        public List<AbstractSweet> Gift  // - СВОЙСТВО!!!!!!!!
        {
            get { return _gift; }
            set { _gift = value; }
        }
        public void Edit(int index, AbstractSweet candy)
        {
            _gift[index] = candy;
        }
        public void Delete(int index)
        {
            _gift.RemoveAt(index);
        }
        public AbstractSweet GetElementByIndex(int index)
        {
            return _gift[index];
        }
        public int CountThickness(AbstractSweet sweet)
        {
            int a = sweet.Weight;
            return a;
        }
    }
}
