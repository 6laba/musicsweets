﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entities;
using logic;

namespace GiftApplic
{
    public partial class MainForm : Form
    {
        private static Present giftik = Factory.Create();
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            foreach (var candy in giftik.Gift)
            {
                listBox1.Items.Add(candy.Name + " " + candy.Weight);
            }

            long Massa = CounterWeight.CountWeight(giftik);
            label2.Text = Massa.ToString();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm();
            if (addForm.ShowDialog(this) == DialogResult.OK)
            {
                AbstractSweet newSw = addForm.candy;
                listBox1.Items.Add(newSw.Name + " " + newSw.Weight);
                giftik.AddSweet(newSw);

                long Massa = CounterWeight.CountWeight(giftik);
                label2.Text = Massa.ToString();
            }
        }

        private void EditBtn_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItems.Count != 0)
            {
                int index = listBox1.Items.IndexOf(listBox1.SelectedItems[0]);
                AddForm addForm = new AddForm(giftik.GetElementByIndex(index));

                if (addForm.ShowDialog(this) == DialogResult.OK)
                {
                    AbstractSweet newSw = addForm.candy;
                    listBox1.Items[index] = newSw.Name.ToString() + " " + newSw.Weight.ToString();
                    giftik.Edit(index, newSw);
                

                    long Massa = CounterWeight.CountWeight(giftik);
            label2.Text = Massa.ToString();
                }

            }
        }

        private void DelBtn_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItems.Count != 0)
            {
                giftik.Delete(listBox1.Items.IndexOf(listBox1.SelectedItems[0]));
                listBox1.Items.Remove(listBox1.SelectedItems[0]);
                long Massa = CounterWeight.CountWeight(giftik);
                label2.Text = Massa.ToString();
            }
        }
    }
}
