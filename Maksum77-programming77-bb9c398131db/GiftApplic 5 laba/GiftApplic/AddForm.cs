﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entities;
using logic;

namespace GiftApplic
{
    public partial class AddForm : Form
    {
        public AddForm()
        {
            InitializeComponent();
        }
        public AddForm(AbstractSweet candy)
        {
            InitializeComponent();
            Name1.Text = candy.Name.ToString();
            Weight.Text = candy.Weight.ToString();
            if (candy is CaramelSweet)
            {
                CaramelSweet caramelka = (CaramelSweet)candy;
                hardness.Text = caramelka.hardness.ToString();
            }
            if (candy is ChokolateSweet)
            {
                ChokolateSweet caramelka = (ChokolateSweet)candy;
                strew.Text = caramelka.strew.ToString();
            }
            if (candy is GlazedSweet)
            {
                GlazedSweet caramelka = (GlazedSweet)candy;
                ThinkofGlaze.Text = caramelka.ThiknessOfGlaze.ToString();
            }
            if (candy is Truffle)
            {
                Truffle caramelka = (Truffle)candy;
                diametr.Text = caramelka.diameter.ToString();
            }


        }

        private void AddForm_Load(object sender, EventArgs e)
        {

        }
        private AbstractSweet _candy;
        public AbstractSweet candy
        {
            get { return _candy; }
            set { _candy = value; }
        }

        private void OkBtn_Click(object sender, EventArgs e)
        {
            switch (classes.Text)
            {
                case "CaramelSweet":
                    _candy = new CaramelSweet(Name1.Text, Int32.Parse(Weight.Text),Int32.Parse(hardness.Text));
                    break;
                case "ChokolateSweet":
                    _candy = new ChokolateSweet(Name1.Text, Int32.Parse(Weight.Text), strew.Text);
                    break;
                case "GlazedSweet":
                    _candy = new GlazedSweet(Name1.Text, Int32.Parse(Weight.Text),Int32.Parse(hardness.Text), Int32.Parse(ThinkofGlaze.Text));
                    break;
                case "Truffle":
                    _candy = new Truffle(Name1.Text, Int32.Parse(Weight.Text), Int32.Parse(diametr.Text));
                    break;
                default:
                    MessageBox.Show("Выбран несуществующий класс");
                    break;
            }
        }
    }
}
