﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using logic;

namespace logic
{
    public class WeightPrinter
    {
        public static void WeightPrint(Present gift)
        {
             
            Console.WriteLine("Weight of the gift = {0}\n", CounterWeight.CountWeight(gift));
            Console.ReadKey();
        }
    }
}
