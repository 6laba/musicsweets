﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace logic
{
   public static class Factory
    {
        public static Present Create()
        {
            Present _giftik = new Present();

            _giftik.AddSweet(new CaramelSweet("Caramelka", 22, 12));
            _giftik.AddSweet(new Truffle("Truffel", 30, 14));
            _giftik.AddSweet(new GlazedSweet("Golybi", 25, 11, 5));
            _giftik.AddSweet(new ChokolateSweet("Chockolatd", 15, "Jam"));

            return _giftik;
        }
    }
}
